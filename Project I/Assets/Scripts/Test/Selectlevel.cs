﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Selectlevel : MonoBehaviour
{
    public Button Level1;
    public Button Level2;

    public void Start()
    {
        Level1.onClick.AddListener(level1);
        Level2.onClick.AddListener(level2);
    }

    private void level2()
    {
        SceneManager.LoadScene("Level2");
    }

    private void level1()
    {
        SceneManager.LoadScene("Game");
    }
}