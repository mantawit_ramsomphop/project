﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Test
{
    public class TestButton : MonoBehaviour
    {
        public Button NextLevel;
        public Button Back;

        public void Start()
        {
            NextLevel.onClick.AddListener(OnnextButtonClicked);
            Back.onClick.AddListener(OnbackButtonClicked);
        }

        private void OnnextButtonClicked()
        {
            SceneManager.LoadScene("Selectlevel");
        }

        private void OnbackButtonClicked()
        {
            SceneManager.LoadScene("Game");
        }
    }
}
